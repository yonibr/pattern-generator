
public class Formula {

	static final char[] ALPHABET_LOWER = { 'a', 'b', 'c', 'd', 'e', 'f', 'g',
			'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
			'u', 'v', 'w', 'x', 'y', 'z' };
	static final char[] ALPHABET_UPPER = { 'A', 'B', 'C', 'D', 'E', 'F', 'G',
			'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
			'U', 'V', 'W', 'X', 'Y', 'Z' };
	static final int iterations = 100;
	private Term[] formula;

	public Formula(char[] seed) {
		formula = new Term[seed.length];
		for (int i = 0; i < seed.length; i++)
			formula[i] = new Term(seed[i], seed.length - i);

	}

	public int getResult(int x, int y)
	{
		return recurse(x, 0) + recurse(y, 0);
	}

	public int recurse(int val, int iteration) {
		if (iteration > iterations)
			return val;
		int returnVal = 0;
		for (int i = 0; i < formula.length; i++) {
			returnVal += formula[i].calculate(val);
		}
		return recurse(returnVal, iteration + 1);
	}

	class Term {
		int power;
		double coefficient;

		public Term(char seed, int index) {
			power = index;
			coefficient = charToInt(seed);
		}

		public int calculate(int input) {
			return (int) (Math.pow(input, power) * coefficient);
		}

		private double charToInt(char seed) {
			for (int i = 0; i < 26; i++)
				if (ALPHABET_LOWER[i] == seed)
					return -1 - i/26.0;
				else if (ALPHABET_UPPER[i] == seed)
					return 1 + i/26.0;

			return 0; // not a letter

		}
	}
}
