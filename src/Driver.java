import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Arrays;
import java.util.Scanner;

import javax.swing.JFrame;

public class Driver extends JFrame {

	public static final int[] RESOLUTION = { 500, 500 };

	private int grid[][] = new int[RESOLUTION[0]][RESOLUTION[1]];

	private boolean paint = true;

	public Driver() {
		setSize(RESOLUTION[0], RESOLUTION[1]);
		setVisible(true);

		new Thread(new Runnable() {

			@Override
			public void run() {
				while (paint) {
					repaint();
					try {
						Thread.sleep(250);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();

		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter a word or phrase (no spaces): ");
		String string = scanner.next();
		char[] seed = string.toCharArray();

		Formula formula = new Formula(seed);

		for (int x = 0; x < RESOLUTION[0]; x++) {
			for (int y = 0; y < RESOLUTION[1]; y++) {
				grid[x][y] = formula.getResult(x - RESOLUTION[0] / 2, y
						- RESOLUTION[1] / 2);
				// System.out.print(grid[x][y] + "   ");

			}
			System.out.println(x);
		}

		paint = false;
		repaint();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D graphics2d = (Graphics2D) g;
		for (int x = 0; x < RESOLUTION[0]; x++) {
			for (int y = 0; y < RESOLUTION[1]; y++) {
				graphics2d.setColor(getColor(grid[x][y]));
				graphics2d.drawLine(x, y, x, y);
			}
		}
	}

	public Color getColor(int i) {
//		i = Math.abs(i);
//		i %= 0xFFFFFF;

//		if (i < 5)
//			i = 0xFFFFFF;
		return new Color(i);
	}

	public static void main(String[] args) {
		new Driver();
	}

}
